package com.r1chjames.videolibrary.models

data class Bucket(val bucketId: String, val bucketName: String, val bucketType: String, val bucketInfo: Map<String, String>)