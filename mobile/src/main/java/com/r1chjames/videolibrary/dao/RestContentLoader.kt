package com.r1chjames.videolibrary.dao

import android.database.Observable
import com.r1chjames.videolibrary.models.Bucket
import com.r1chjames.videolibrary.models.File
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import javax.xml.transform.Result

interface RestContentLoader {

    @GET("buckets")
    fun getBuckets(
            @Header("accountId") accountId: String,
            @Header("applicationKey") applicationKey: String): Call<List<Bucket>>

    @GET("files/{bucketId}")
    fun listFilesInBucket(
            @Header("accountId") accountId: String,
            @Header("applicationKey") applicationKey: String,
            @Path("bucketId") bucketId: String): Call<List<File>>

    @GET("files/{bucketId}/stream/{fileId}")
    fun streamFile(
            @Header("accountId") accountId: String,
            @Header("applicationKey") applicationKey: String,
            @Path("bucketId") bucketId: String,
            @Path("fileId") fileId: String): Observable<Result>


    companion object Factory {
        val BASE_URL = "http://10.0.2.2:9000/api/v1/"
        fun create(): RestContentLoader {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(RestContentLoader::class.java)
        }
    }
}
