package com.r1chjames.videolibrary

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ListView
import com.r1chjames.videolibrary.adapters.BucketListViewAdapter
import com.r1chjames.videolibrary.dao.RestContentLoader
import com.r1chjames.videolibrary.models.Bucket
import retrofit2.Call
import retrofit2.Callback


class MainActivity : AppCompatActivity() {

    lateinit var bucketList: List<Bucket>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = "Buckets"

        val bucketListView = findViewById<ListView>(R.id.bucket_list_view)

        callWebService(this, bucketListView)

        bucketListView.setOnItemClickListener {_, _, position, _ ->
            val selectedBucket = bucketList[position].bucketId
            val fileIntent = Intent(this, FilesActivity::class.java)
            fileIntent.putExtra("bucketId", selectedBucket)

            startActivity(fileIntent)
        }


    }

    private fun callWebService(context: Context, bucketListView: ListView) {
        //TODO following this https://androidteachers.com/kotlin-for-android/retrofit-kotlin-android/
        val retrofitService = RestContentLoader.create()

        val call = retrofitService.getBuckets("60038c803627","0012ae0c6e4217dd05a924eae1b7a4db42b47cc7b5")

        Log.d("REQUEST", call.toString() + "")

        call.enqueue(object : Callback<List<Bucket>> {
            override fun onResponse(call: Call<List<Bucket>>, response: retrofit2.Response<List<Bucket>>?) {
                if (response != null) {
                    bucketList = response.body()!!.toList()
                    Log.d("MainActivity", "" + bucketList.size)

                    val arrayAdapter = BucketListViewAdapter(
                            context,
                            bucketList)


                    bucketListView.adapter = arrayAdapter
                }
            }

            override fun onFailure(call: Call<List<Bucket>>, t: Throwable) {
                Log.e("Error", t.message)
            }
        })

    }

    fun fakeBuckets(): List<Bucket> {
        val bucketList = ArrayList<Bucket>()
        bucketList.add(Bucket("abc1234", "BucketJava 1", "big", emptyMap()))
        bucketList.add(Bucket("abc4654", "BucketJava 2", "big", emptyMap()))
        bucketList.add(Bucket("abc6546", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc7787", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc6545", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc9987", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc1154", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc4567", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc2254", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc1214", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc7849", "BucketJava 3", "big", emptyMap()))
        bucketList.add(Bucket("abc4556", "BucketJava 3", "big", emptyMap()))
        return bucketList
    }
}
