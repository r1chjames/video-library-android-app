package com.r1chjames.videolibrary.models

data class FileMetaData(val contentLength: Long, val contentType: String, val contentSha1: String, val fileInfo: Map<String, String>)
