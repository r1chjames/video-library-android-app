package com.r1chjames.videolibrary

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ListView
import com.r1chjames.videolibrary.adapters.FileListViewAdapter
import com.r1chjames.videolibrary.dao.RestContentLoader
import com.r1chjames.videolibrary.models.File
import com.r1chjames.videolibrary.models.FileTypeEnumerationJava
import retrofit2.Call
import retrofit2.Callback

class FilesActivity : AppCompatActivity() {

    lateinit var fileList: List<File>

    override fun onCreate(savedInstanceState: Bundle?) {

        val bundle: Bundle? = intent.extras
        val bucketId: String? = intent.getStringExtra("bucketId")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_files)
        title = bucketId

        val fileListView = findViewById<ListView>(R.id.files_list_view)

        callWebService(bucketId!!, this, fileListView)

        fileListView.setOnItemClickListener {_, _, position, _ ->
            val fileIntent = Intent(this, VideoActivity::class.java)
            fileIntent.putExtra("fileUrl", fileList[position].url)

            startActivity(fileIntent)
        }

    }

    fun callWebService(bucketId: String, context: Context, fileListView: ListView) {
        //TODO following this https://androidteachers.com/kotlin-for-android/retrofit-kotlin-android/
        val retrofitService = RestContentLoader.create()

        val call = retrofitService.listFilesInBucket("60038c803627","0012ae0c6e4217dd05a924eae1b7a4db42b47cc7b5", bucketId)

        Log.d("REQUEST", call.toString() + "")

        call.enqueue(object : Callback<List<File>> {
            override fun onResponse(call: Call<List<File>>, response: retrofit2.Response<List<File>>?) {
                if (response != null) {
                    fileList = response.body()!!.toList()
                    Log.d("MainActivity", "" + fileList.size)

                    val arrayAdapter = FileListViewAdapter(
                            context,
                            fileList)

                    fileListView.adapter = arrayAdapter
                }
            }

            override fun onFailure(call: Call<List<File>>, t: Throwable) {
            }
        })

    }


    fun fakeFiles(): List<File> {
        val fileList = ArrayList<File>()
        fileList.add(File("File 1", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 2", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 3", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 4", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 5", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 6", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 7", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 8", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 9", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 10", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 11", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 12", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 13", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 14", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 15", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 16", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 17", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        fileList.add(File("File 18", "fileabcd", FileTypeEnumerationJava.VIDEO, ""))
        return fileList
    }

}
