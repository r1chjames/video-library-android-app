package com.r1chjames.videolibrary.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.r1chjames.videolibrary.R
import com.r1chjames.videolibrary.models.File

class FileListViewAdapter(private val context: Context,
                          private val dataSource: List<File>) : BaseAdapter() {


    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.list_item_file, parent, false)

        val titleTextView = rowView.findViewById(R.id.file_list_title) as TextView
        val subtitleTextView = rowView.findViewById(R.id.file_list_subtitle) as TextView
        val detailTextView = rowView.findViewById(R.id.file_list_metadata) as TextView

        val file = getItem(position) as File
        titleTextView.text = file.fileName
        subtitleTextView.text = file.fileId
        detailTextView.text = "TBC"

//        Picasso.with(context).load(bucket.imageUrl).placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)

        return rowView
    }
}