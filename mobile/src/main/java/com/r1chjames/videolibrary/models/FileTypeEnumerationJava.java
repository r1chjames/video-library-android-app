package com.r1chjames.videolibrary.models;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum FileTypeEnumerationJava {

    VIDEO("video");

    private String value;
}
