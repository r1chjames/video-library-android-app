package com.r1chjames.videolibrary.models

data class File(val fileName: String, val fileId: String, val fileType: FileTypeEnumerationJava, val url: String)